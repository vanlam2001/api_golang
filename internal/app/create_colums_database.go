package app

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = ""
	dbname   = "test"
)

func Create_Colums_Database() {
	// Tạo chuỗi kết nối
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	// Kết nối đến database
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	fmt.Println("Đã kết nối đến database ")

	// Thực hiện truy vấn để tạo bảng
	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS users (
			id serial PRIMARY KEY,
			name VARCHAR (255),
			email VARCHAR (255)
		);
	`)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Đã tạo bảng database thành công")
}
