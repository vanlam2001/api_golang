package api

import (
	"encoding/json"
	"net/http"
)

// Tạo một struct để đại diện cho dữ liệu bạn muốn render
type Data struct {
	Message string `json:"message"`
}

func List_Render() {
	// Định nghĩa một endpoint "/api/data" để trả về dữ liệu JSON
	http.HandleFunc("/api/data", func(w http.ResponseWriter, r *http.Request) {
		// Tạo một instance của struct Data và cung cấp dữ liệu
		data := Data{
			Message: "Hello, World!",
		}

		// Chuyển đổi dữ liệu thành JSON
		jsonData, err := json.Marshal(data)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Thiết lập tiêu đề Content-Type cho Response là JSON
		w.Header().Set("Content-Type", "application/json")

		// Gửi dữ liệu JSON cho client
		w.Write(jsonData)
	})

	// Khởi động server trên cổng 8080
	http.ListenAndServe(":8000", nil)
}
