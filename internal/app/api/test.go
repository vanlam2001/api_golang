package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func Test() {
	// Khởi tạo router của Gin
	r := gin.Default()

	// Định nghĩa router cho API "Test"

	r.GET("/test", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Test",
		})
	})

	// Chạy server trên cổng 8000
	if err := r.Run(":8000"); err != nil {
		panic(err)
	}
}
