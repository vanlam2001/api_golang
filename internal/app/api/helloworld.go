package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func HelloWorld() {
	// Khởi tạo router của Gin
	r := gin.Default()

	// Định nghĩa route cho API "Hello World"

	r.GET("/hello", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "Hello World",
		})
	})

	// Chạy server trên cổng 8000
	if err := r.Run(":8000"); err != nil {
		panic(err)
	}
}
