package api

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/lib/pq"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "0903992237"
	dbname   = "test"
)

// Define a User model
type User struct {
	gorm.Model
	Username string
	Password string
}

func Register() {
	// Khởi tạo router
	router := gin.Default()

	// Kết nối đến cơ sở dữ liệu PostgreSQL
	db, err := gorm.Open("postgres",
		"host="+host+
			" port="+fmt.Sprintf("%d", port)+
			" user="+user+
			" password="+password+
			" dbname="+dbname+
			" sslmode=disable")
	if err != nil {
		panic("Không thể kết nối đến cơ sở dữ liệu")
	}
	defer db.Close()

	// Tạo bảng User trong database (nếu chưa tồn tại)
	db.AutoMigrate(&User{})

	// Đăng ký tài khoản - Đây là route bạn đang thiếu
	router.POST("/dang-ky", func(c *gin.Context) {
		var user User
		if err := c.ShouldBindJSON(&user); err != nil {
			c.JSON(400, gin.H{"error": err.Error()})
			return
		}

		// Lưu tài khoản vào database
		db.Create(&user)

		c.JSON(200, gin.H{"message": "Đăng ký tài khoản thành công"})
	})

	router.Run(":8000")
}
