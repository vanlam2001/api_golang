package main

import (
	"golang_sql/internal/app/api"
)

func main() {
	api.HelloWorld()
	api.Register()
	api.List_Render()
	api.Test()
}
